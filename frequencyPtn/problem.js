// Write a function called same, 
// which accepts two arrays. 
// The function should return true 
// if every value in the array has 
// it’s corresponding value squared in the second array. 
// The frequency of values must be the same.

// solution 1
function same(arr1, arr2) {
  if (arr1.length !== arr2.length) { 
    return false;
  }

  for (let i = 0; i < arr1.length; i++) {
    const indexNum = arr2.indexOf(arr1[i] ** 2);
    if (indexNum === -1) {
      return false;
    }
    arr2.splice(indexNum, 1);
  }
  return true;
}


// solution 2
function sameArr(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    return false;
  }

  const frequencyCounter1 = {};
  const frequencyCounter2 = {};

  for (let val of arr1) {
    frequencyCounter1[val] = ++frequencyCounter1[val] || 0;
  }
  for (let val of arr2) {
    frequencyCounter2[val] = ++frequencyCounter2[val] || 0;
  }

  for (let key in frequencyCounter1) {
    if (!(key ** 2 in frequencyCounter2)) {
      return false;
    }

    if (frequencyCounter1[key] !== frequencyCounter2[key ** 2]) {
      return false;
    }
  }
  
  return true;
}

console.log(sameArr([1, 2, 1], [1, 1, 4]));