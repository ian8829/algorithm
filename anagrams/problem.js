// Given two strings, write a function 
// to determine if the second string is 
// an anagram of the first. 
// An anagram is a word, phrase, 
// or name formed by rearranging the letters of another, 
// such as cinema, formed from iceman.


// my solution 1
function validAnagram(str1, str2) {
  if (str1.length !== str2.length) {
    return false;
  }

  const checkCharCount1 = {};
  const checkCharCount2 = {};

  for (let char of str1) {
    checkCharCount1[char] = ++checkCharCount1[char] || 1;
  }

  for (let char of str2) {
    checkCharCount2[char] = ++checkCharCount2[char] || 1;
  }

  for (let key in checkCharCount1) {
    if (checkCharCount1[key] !== checkCharCount2[key]) {
      return false;
    }
  }
  return true;
}

// solution 2
function validAnagramSecond(first, second) {
  if (first.length !== second.length) {
    return false;
  }

  const lookup = {};

  for (i = 0; i < first.length; i++) {
    const letter = first[i];
    // lookup[letter] ? lookup[letter] += 1 : lookup[letter] = 1;
    lookup[letter] = ++lookup[letter] || 1;
  }

  for (i = 0; i < second.length; i++) {
    const letter = second[i];
    if (!lookup[letter]) {
      return false;
    } else {
      lookup[letter] -= 1;
    }
  }

  return true;
}

// console.log(validAnagram('anagram', 'marganaa'));
console.log(validAnagramSecond('anagram', 'margana'));