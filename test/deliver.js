// n = 15? n/3 = 5, n/5 = 3 => return 3
// n = 60? n/3 = 20, n/5 = 12 =>  return 12
// n = 7? n/3 X , n/5 X => return -1

const solution = n => {
    let answer = -1;
    if (Number.isInteger(n/3) || Number.isInteger(n/5)) {
        if (Number.isInteger(n/3) && !Number.isInteger(n/5)) {
            answer = n/3;
            return answer;
        } 
        answer = n/5
        return answer;
    } 
    return answer;
}

console.log(solution(27));