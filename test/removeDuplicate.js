// [1, 5, 8, 2, 10, 5, 4, 6, 4, 8]	=> [1, 5, 8, 2, 10, 4, 6]
// [5, 4, 4, 3, 5] => [5, 4, 3]

const filter = (arr) => {
    const filteredArr = arr.filter((item, index) => arr.indexOf(item) === index);
    return filteredArr;
}
