// window의 this (global)
const helloWorld = () => console.log(this, 'Hello');

helloWorld();


// object 내 value에 접근하는 method
const Portland = {
    birdges: 12,
    airport: 1,
    soccerTeams: 1,
    logNumberOfBridges() {
        console.log(`There are ${this.birdges} bridges in Portland!`);
    }
}

Portland.logNumberOfBridges();

// 메소드를 프로토 타입에 할당하는 경우
function logTeams() {
    console.log(this.soccerTeams);
}

Portland.foo = logTeams;
Portland.foo();

// constructor function의 value 할당시
const City = function(name, state) {
    this.name = name || 'Portland';
    this.state = state || 'Oregon';
    this.printMyCityAndState = function() {
        console.log(`My city is ${this.name}, and my state is ${this.state}`);
    }
};

portland = new City();
seattle = new City('Seattle', 'Washington');

portland.printMyCityAndState();
seattle.printMyCityAndState();