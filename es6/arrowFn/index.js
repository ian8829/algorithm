const team = {
    members: ['Jane', 'Bill'],
    teamName: 'Super Squad',
    teamSummary: function() {
        return this.members.map(function(member) {
            return `${member} is on team ${this.teamName}`;
        });
    }
}

// console.log(team.teamSummary());
// teamName is undefined. this.teamName, because 'this' is disappeared

// ===========>


const teamSecond = {
    members: ['Jane', 'Bill'],
    teamName: 'Super Squad',
    teamSummary: function() {
        return this.members.map(function(member) {
            return `${member} is on team ${this.teamName}`;
        }.bind(this));
    }
}

// console.log(teamSecond.teamSummary());
// [ 'Jane is on team Super Squad', 'Bill is on team Super Squad' ]



const teamThird = {
    members: ['Jane', 'Bill'],
    teamName: 'Super Squad',
    teamSummary: function() {
        return this.members.map((member) => `${member} is on team ${this.teamName}`);
    }
}

console.log(teamThird.teamSummary());
// arrow function use lexical this
// this === team
// [ 'Jane is on team Super Squad', 'Bill is on team Super Squad' ]

