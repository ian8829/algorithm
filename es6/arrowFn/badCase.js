// arrow function은 lexical this를 지원하므로 콜백함수로 사용하기 편하다.
// lexical this (화살표 함수는 함수를 선언할때 this에 바인딩할 객체가 정적으로 결정된다.
// 동적으로 결정되는 일반 함수와는 달리 화살표 함수의 this는 언제나 상위 스코프의 this를 가리킨다)
// 하지만, 화살표 함수를 사용하는것이 오히려 혼란을 불러 올 수 있다.

// 1. 메소드

// Bad
const person = {
    name: 'Lee',
    sayHi: () => console.log(`Hi ${this.name}`)
};
// person.sayHi();
// Hi undefined(this.name)
// 위의 경우 this는 상위컨텍스트인 전역객체 window를 가리킨다.

// Good
const personSecond = {
    name: 'Lee',
    sayHi() {
        console.log(`Hi ${this.name}`);
    }
}

// personSecond.sayHi();
// Hi Lee
// 축약 메소드 표현 사용



// 2. protoType

// Bad
const personProto = {
    name: 'Lee'
};

Object.prototype.sayHi = () => console.log(`Hi ${this.name}`);
// personProto.sayHi();
// Hi undefined
// 위의 경우 this는 상위컨텍스트인 전역객체 window를 가리킨다.

// Good
const personProtoSecond = {
    name: 'Lee'
};

Object.prototype.sayHello = function () {
    console.log(`Hi ${this.name}`);
}

// personProtoSecond.sayHello();
// Hi Lee


// 3. 생성자 함수

// bad
// const Foo = () => {};

// console.log(Foo.hasOwnProperty('prototype'));
// // false

// const foo = new Foo();
// // TypeError: Foo is not a constructor
// 생성자 함수는 prototype 프로퍼티를 가지며, 
// prototype 프로퍼티가 가리키는 프로토타입 객체의 constructor를 사용한다.
// 하지만 arrow function은 protype property를 가지고 있지 않다.
