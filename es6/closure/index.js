// global variable 을 공유함에 따라 생기는 문제
// let count = 0;

// function countBirds() {
//     count += 1;
//     return console.log(`${count} birds`);
// }

// function countDogs() {
//     count += 1;
//     return console.log(`${count} dogs`);
// }

// countBirds();
// countBirds();
// countBirds();
// countBirds();
// countDogs();
// countBirds();



// Closure
let birds = 3;

function dogHouse() {
    const dogs = 8;
    function showDogs() {
        console.log(dogs);
    }
    return showDogs;
}

const getDogs = dogHouse();

// getDogs();


// Closure 적용
let count = 0;

const makeCounts = (noun) => {
    let count = 0;
    return function() {
        count += 1;
        return console.log(`${count} ${noun}`);
    }
}

const birdCounter = makeCounts('birds');
const dogCounter = makeCounts('dogs');

birdCounter();
birdCounter();
birdCounter();
dogCounter();
dogCounter();
birdCounter();
