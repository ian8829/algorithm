// Write a function called sumZero 
// which accepts a sorted array of integers. 
// The function should find the first pair 
// where the sum is 0. Return an array 
// that includes both values 
// that sum to zero or undefined 
// if a pair does not exist.

// my solution
function sumZero(arr) {
  if (arr.length % 2 === 1 && arr[(arr.length - 1)/ 2] !== 0) {
    return undefined;
  }

  for (fromStart = 0; fromStart < Math.floor(arr.length / 2); fromStart++) {
    const fromEnd = arr.length - 1 - fromStart;
    if (!(arr[fromStart] + arr[fromEnd] === 0)) {
      return undefined;
    }
  }
  return [arr[0], arr[arr.length - 1]];
}

console.log(sumZeroThree([-5, -2, -1, 0, 1, 2, 3]));

// solution 2
function sumZeroTwo(arr) {
  for (i = 0; i < arr.length; i++) {
    for(j = i + 1; j < arr.length; j++) {
      if (arr[i] + arr[j] === 0) {
        return [arr[i], arr[j]];
      }
    }
  }
}

// solution 3
function sumZeroThree(arr) {
  let left = 0;
  let right = arr.length - 1;
  while (left < right) {
    const sum = arr[left] + arr[right];
    if (sum === 0) {
      return [arr[left], arr[right]];
    } else if (sum > 0) {
      right--;
    } else {
      left++;
    }
  }
}