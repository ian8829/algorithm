const { performance } = require('perf_hooks');

// Suppose we want to write a function that
// calculates the sum of all numbers 
// from 1 up to (and including) some number n.

// solution 1
function addUpTo(n) {
  let total = 0;
  for (let i = 0; i <= n; i++) {
    total += i;
  }
  return total;
}

// solution 2
const addUpToSecond = n => n * (n + 1) / 2;


// count up and down
function countUpAndDown(n) {
  console.log("Going up!");
  for (let i = 0; i < n; i++) {
    console.log(i);
  }
  console.log("At the top!\nGoing down...");
  for (let j = n - 1; j >= 0; j--) {
    console.log(j);
  }
  console.log("Back down. Bye!");
}

// print all pairs
function printAllPairs(n) {
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      console.log(i, j);
    }
  }
}

// log at least 5
function logAtLeast(n) {
  for (let i = 1; i <= Math.max(5, n); i++) {
    console.log(i);
  }
}

// log at most 5
function lostAtMost5(n) {
  for (let i = 1; i <= Math.min(5, n); i++) {
    console.log(i);
  }
}

// sum array
function sum(arr) {
  let total = 0;
  for (let i = 0; i < arr.length; i++) {
    total += arr[i];
  }
  console.log(`total? ${total}`);
  return total;
}

// sum double array
function double(arr) {
  let newArr = [];
  for (let i = 0; i < arr.length; i++) {
    newArr.push(2 * arr[i]);
  }
  console.log(`newArr? ${newArr}`);
  return newArr;
}

// calculate time
const t1 = performance.now();
// addUpTo(100000000);
double([1, 5, 7]);
const t2 = performance.now();
console.log(`Time Elapsed: ${(t2 - t1) / 1000} seconds.`);