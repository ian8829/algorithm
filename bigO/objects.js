// Objects, key value pair
const instructor = {
  firstName: "Kelly",
  isInstructor: true,
  favoriteNumbers: [1, 2, 3, 4]
};

// console.log(Object.entries(instructor));
// console.log(instructor.hasOwnProperty("isInstructor"));

// arrays
const names = ["Michael", "Melissa", "Andrea"];

