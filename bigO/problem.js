// Write a function which takes two numbers and 
// returns their sum.

function sum(num1, num2) {
  return num1 + num2;
}

// Write a function which takes in a string and
// returns counts of each character in the string

// make object to return at end
// loop over string, for each character...
  // if the char is a number/letter AND is a key in object, add one to count
  // if not, add it and set value to 1
  // if character is something else (space, period, etc..) don't do anything
// return object at end

//   // simple solution 1
// function countChar(str) {
//   const result = {};
//   for (let i = 0; i < str.length; i++) {
//     const char = str[i].toLowerCase();
//     if (/[a-z0-9]/.test(char)) {
//       if (result[char] > 0)  {
//         result[char]++;
//       } else {
//         result[char] = 1;
//       };
//     }
//   }
//   return result;
// }

// // simple solution 2
// function countChar(str) {
//   const obj = {};
//   for (let char of str) {
//     char = char.toLowerCase();
//     if (/[a-z0-9]/.test(char)) {
//       // if true ++obj[char], false 1
//       obj[char] = ++obj[char] || 1;
//     }
//   }
//   return obj;
// }

// simple solution 3
function isAlphaNumeric(char) {
  const code = char.charCodeAt(0);
  if (!(code > 47 && code < 58) && // numeric (0-9)
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
        return false;
      }
      return true;
}

function countChar(str) {
  const obj = {};
  for (let char of str) {
    if (isAlphaNumeric(char)) {
      char = char.toLowerCase();
      obj[char] = ++obj[char] || 1;
    }
  }
  return obj;
}

console.log(countChar('Hellohh dfd34?/'));