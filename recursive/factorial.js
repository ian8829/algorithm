// writing factorial iteratively
const factorial = num => {
    let total = 1;
    for (let i = num; i > 0; i--) {
        total *= i;
        console.log(`i? ${i}, total? ${total}`);
    }
    return total;
}

// console.log(factorial(4));


// writing factorial recursively
const factorialR = num => {
    if (num === 1) return 1;
    return num * factorialR(num -1)
}

console.log(factorialR(2));
