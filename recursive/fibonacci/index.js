// fibonacci 
// 0, 1, 1, 2, 3, 5, 8, 13 ...
// arr[0] = 0, arr[1] = 1, arr[2] = 1, arr[3] = 2
// arr[2] = arr[1] + arr[0] = 0 + 1 = 2;
// arr[3] = arr[2] + arr[1] = 2 + 1 = 3;

const fib = (n) => {
    let arr = [0, 1];
    for (let i = 2; i < n + 1; i++) {
        arr.push(arr[i - 1] + arr[i -2]);
    }
    console.log(`n? ${n}, arr? ${arr}, arr[n]? ${arr[n]}`)
    return arr[n];
}

// fib(10);

// fib recursive
const fibR = n => {
    if (n === 0) {
        return 0;
    } else if (n === 1) {
        return 1;
    } 
    return fibR(n - 2) + fibR(n - 1);
}


// fibR(2) 
console.log(fibR(6));