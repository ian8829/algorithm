const countDown = (num) => {
    // base case countDown(0)
    if (num <= 0) {
        console.log("All done!");
        return;
    }

    // different input
    console.log(`num? ${num}`);
    num--;
    countDown(num);
}

countDown(8);