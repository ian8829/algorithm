// Implement a function called countUniqueValues, 
// which accepts a sorted array, 
// and counts the unique values in the array. 
// There can be negative numbers in the array, 
// but it will always be sorted.

// my solution 1
function countUniqueValues(arr) {
  let obj = {};
  for (i = 0; i < arr.length; i++) {
    obj[arr[i]] = obj[arr[i]] + 1 || 0; 
  }
  return Object.keys(obj).length;
}

// solution 2
function countUniqueValuesSecond(arr) {
  if (!arr.length) {
    return 0;
  }
  let i = 0;
  for (j = 0; j < arr.length; j++) {
    if (arr[i] !== arr[j]) {
      i++;
      arr[i] = arr[j];
    }
    console.log(`i? ${i}, j? ${j}, arr? ${arr}`);
  }
  return i + 1;
}

console.log(countUniqueValues([1, 1, 1, 1, 3, 3, 5, 7, 8, 9]));
console.log(countUniqueValuesSecond([1, 1, 1, 1, 1, 3, 3, 5, 7, 8, 9]));